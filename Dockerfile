FROM python:3-alpine
RUN apk add python3-dev build-base linux-headers postgresql-dev
COPY requirements.txt /
RUN pip install -r /requirements.txt
RUN mkdir /code
WORKDIR /code
ADD . /code/
ENV PYTHONUNBUFFERED 1
CMD uwsgi uwsgi.ini
