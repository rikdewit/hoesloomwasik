from flask import Flask
from app.config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate



db = SQLAlchemy(session_options={'expire_on_commit':False})
migrate = Migrate()


def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)
    db.init_app(app)
    migrate.init_app(app, db)

    return app

app = create_app()

from flask import Flask, render_template, request, redirect
from bs4 import BeautifulSoup
import requests
import re
import time

@app.route('/',methods=['GET'])
def index():
        races = {"tt":scrape1(),"iris":scrape2()}
        return render_template('index.html', races=races)



def scrape1():
    tt_url = "https://time-team.nl/en/info"

    res = requests.get(tt_url)
    soup = BeautifulSoup(res.content, "html5lib")
    races = soup.find_all('div',class_="category")[:3]

    results = []
    for race in races:
        name = race.find('h2').text
        link = race.find(href=re.compile("results")).get('href')
        results.append({"name":name,"link":link})

    return results

def scrape2():
    hs_url = "https://hoesnelwasik.nl"
    races = reversed(requests.get(hs_url+"/api/").json()["regattas"][-5:])
    print(races)
    results = []
    for race in races:
        shortname = race["shortname"]
        year = race["jaar"][-2:]

        r = {}
        r["link"]=f"{hs_url}/{shortname}/{year}"
        r["shortname"] = shortname
        r["year"] = year
        r["name"]=race["regattaname"]
        results.append(r)

    return results
