import os

class Config():
    # Postgresql Database
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    if os.path.exists("DEVELOP"):
        os.environ["FLASK_ENV"] = "development"
